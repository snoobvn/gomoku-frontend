package com.example.sprint_retrospective;

public class Constant {
    public final static Integer boardSize = 16;
    public final static Integer winCell = 5;
}
