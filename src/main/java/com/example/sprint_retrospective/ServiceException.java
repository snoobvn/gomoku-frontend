package com.example.sprint_retrospective;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@ResponseStatus(value = HttpStatus.BAD_REQUEST)  // 404
public class ServiceException extends RuntimeException {

    public final static ServiceException USER_NOT_FOUND = new ServiceException(1, "User not found");
    public final static ServiceException USER_EXIST = new ServiceException(2, "User exists");
    public final static ServiceException USER_ACTIVATE = new ServiceException(2, "User need to be activated");
    public final static ServiceException USER_BANNED = new ServiceException(2, "User banned");
    public final static ServiceException MATCH_PLAYING = new ServiceException(2, "You have one match playing, can't start new match");
    public final static ServiceException INVALID_PASSWORD = new ServiceException(3, "Invalid Password");
    public final static ServiceException MATCH_NOT_FOUND = new ServiceException(4, "Board Not Found");
    public final static ServiceException MATCH_INVALID_STATUS = new ServiceException(4, "Board In Invalid Status");
    public final static ServiceException MATCH_INVALID_ACCESS = new ServiceException(4, "This user is not allowed to access this board");
    public final static ServiceException TAG_NOT_FOUND = new ServiceException(5, "Tag Not Found");
    public final static ServiceException CONTENT_EMPTY = new ServiceException(6, "Empty Input Error");

    ServiceException() {
    }

    public ServiceException(long code, String message) {
        this.code = code;
        this.message = message;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private long code;
    private String message;

    public final static class ServiceExceptionResponse {
        private long code;
        private String message;

        public ServiceExceptionResponse(ServiceException serviceException) {
            code = serviceException.getCode();
            message = serviceException.getMessage();
        }

        public long getCode() {
            return code;
        }

        public void setCode(long code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
