package com.example.sprint_retrospective.dto;

import java.util.List;

public class SimpleUser {
    private Integer id;
    private String displayName;
    private Integer score;
    private final Boolean passwordChanged;

    public SimpleUser(User user) {
        this.id = user.getId();
        this.displayName = user.getDisplayName();
        this.score = user.getScore();
        this.passwordChanged = user.getPasswordChanged();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Boolean getPasswordChanged() {
        return passwordChanged;
    }
}
