package com.example.sprint_retrospective.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String displayName;

    private String email;

    private Integer score;

    //Need to activation by mail
    private Boolean enabled = false;

    private Boolean locked = false;

    @JsonIgnore
    private String hashedPassword;

    private Boolean passwordChanged;

    @OneToMany(mappedBy = "player1", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Match> player1Matches = new HashSet<>();

    @OneToMany(mappedBy = "player2", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Match> player2Matches = new HashSet<>();

    public User() {
    }

    public User(String displayName, String email, String hashedPassword) {
        this.displayName = displayName;
        this.email = email;
        this.hashedPassword = hashedPassword;
        this.score = 1000;
        this.locked = false;
        this.enabled = false;
        this.passwordChanged = true;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getScore() {
        return score;
    }

    public void changeScore(Integer score){
        setScore(this.score + score);
    }
    public void setScore(Integer score) {
        this.score = score;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean disabled) {
        this.enabled = disabled;
    }

    public Set<Match> getPlayer1Matches() {
        return player1Matches;
    }

    public void setPlayer1Matches(Set<Match> player1Matches) {
        this.player1Matches = player1Matches;
    }

    public Set<Match> getPlayer2Matches() {
        return player2Matches;
    }

    public void setPlayer2Matches(Set<Match> player2Matches) {
        this.player2Matches = player2Matches;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id);
    }

    public Boolean getPasswordChanged() {
        return passwordChanged;
    }

    public void setPasswordChanged(Boolean passwordChanged) {
        this.passwordChanged = passwordChanged;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
