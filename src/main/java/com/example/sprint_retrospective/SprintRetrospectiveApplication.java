package com.example.sprint_retrospective;

import com.example.sprint_retrospective.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.persistence.EntityManager;

@SpringBootApplication
@EnableAsync
public class SprintRetrospectiveApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SprintRetrospectiveApplication.class, args);
    }

    @Autowired
    EntityManager entityManager;
    @Autowired
    UserService userService;

    @Override
    public void run(String... args) throws Exception {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
//        System
//        User user = new User();
//        user.setUserName("sample");
//        user.setHashedPassword(DigestUtils.md5Hex("sample"));
//        entityManager.persist(user);
//        Board board1 = new Board("Quotes", "My Favorite Quotes", UUID.randomUUID().toString(), user);
//        entityManager.persist(board1);
//        entityManager.flush();
//        board1.addTag(new Tag("When a person cannot deceive himself the chances are against his being able to deceive other people.", board1, Tag.TagType.WENT_WELL));
//        board1.addTag(new Tag("He talked with more claret than clarity.", board1, Tag.TagType.WENT_WELL));
//        board1.addTag(new Tag("Eat a live toad the first thing in the morning and nothing worse will happen to you the rest of the day.", board1, Tag.TagType.WENT_WELL));
//        board1.addTag(new Tag("Nothing you can't spell will ever work.", board1, Tag.TagType.TO_IMPROVE));
//        board1.addTag(new Tag("A brand for a company is like a reputation for a person. You earn reputation by trying to do hard things well.", board1, Tag.TagType.TO_IMPROVE));
//        board1.addTag(new Tag("Hell hath no fury like a bureaucrat scorned.", board1, Tag.TagType.TO_IMPROVE));
//        board1.addTag(new Tag("It was never what I wanted to buy that held my heart's hope. It was what I wanted to be."
//                , board1, Tag.TagType.ACTION_ITEM));
//        board1.addTag(new Tag("Never believe anything until it has been officially denied."
//                , board1, Tag.TagType.ACTION_ITEM));
//        board1.addTag(new Tag("Nothing endures but personal qualities."
//                , board1, Tag.TagType.ACTION_ITEM));
//        entityManager.flush();
//        System.out.println("Yaaaaaaaaaaa");
//    }
}
