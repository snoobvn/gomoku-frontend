package com.example.sprint_retrospective.util;

import com.example.sprint_retrospective.dto.User;
import org.springframework.security.core.Authentication;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public class SpringUtils {
    public static Optional<HttpServletRequest> getCurrentHttpRequest() {
        return
                Optional.ofNullable(
                        RequestContextHolder.getRequestAttributes()
                )
                        .filter(ServletRequestAttributes.class::isInstance)
                        .map(ServletRequestAttributes.class::cast)
                        .map(ServletRequestAttributes::getRequest);
    }

    public static User getUser(Authentication authentication) {
        if (authentication.getDetails() instanceof User) {
            return (User) authentication.getDetails();
        }
        return null;
    }
}
