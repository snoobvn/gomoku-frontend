package com.example.sprint_retrospective.controller.message;

import com.example.sprint_retrospective.dto.Match;

public class MatchEndedMessage extends MatchMessage {
    private Integer matchResult;

    public MatchEndedMessage() {
    }

    public MatchEndedMessage(Integer matchId, Integer matchResult) {
        this.matchResult = matchResult;
    }

    public Integer getMatchResult() {
        return matchResult;
    }

    public void setMatchResult(Integer matchResult) {
        this.matchResult = matchResult;
    }
}
