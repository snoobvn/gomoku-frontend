package com.example.sprint_retrospective.controller.message;

import com.example.sprint_retrospective.dto.Match;
import com.example.sprint_retrospective.logic.Board;
import com.example.sprint_retrospective.logic.MoveResult;

public class MoveMessage extends MatchMessage {
    private Integer i;
    private Integer j;
    private MoveResult moveResult;
    public MoveMessage() {
    }

    public MoveMessage(Integer i, Integer j) {
        this.i = i;
        this.j = j;
    }

    public MoveMessage(Match match, Board board) {
        super(match, board);
    }

    public Integer getI() {
        return i;
    }

    public void setI(Integer i) {
        this.i = i;
    }

    public Integer getJ() {
        return j;
    }

    public MoveResult getMoveResult() {
        return moveResult;
    }

    public void setMoveResult(MoveResult moveResult) {
        this.moveResult = moveResult;
    }

    public void setJ(Integer j) {

        this.j = j;
    }
}
