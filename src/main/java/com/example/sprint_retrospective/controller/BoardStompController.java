package com.example.sprint_retrospective.controller;

import com.example.sprint_retrospective.ServiceException;
import com.example.sprint_retrospective.controller.message.*;
import com.example.sprint_retrospective.dto.ChatLine;
import com.example.sprint_retrospective.dto.Match;
import com.example.sprint_retrospective.dto.SimpleUser;
import com.example.sprint_retrospective.dto.User;
import com.example.sprint_retrospective.logic.MoveResult;
import com.example.sprint_retrospective.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;

import java.util.Set;

@Controller
public class BoardStompController {
    public final static String ONLINE_TOPIC = "/topic/online";
    public final static String START_TOPIC = "/topic/start";
    public final static String RESIGN_TOPIC = "/topic/resign";
    public final static String MOVE_TOPIC = "/topic/move";
    public final static String JOIN_TOPIC = "/topic/join";
    @Autowired
    BoardService boardService;
    @Autowired
    MatchService matchService;
    @Autowired
    ChatLineService chatLineService;
    @Autowired
    OnlineListService onlineListService;
    @Autowired
    UserService userService;

    @MessageMapping("/online")
    @SendTo(ONLINE_TOPIC)
    public Set<SimpleUser> online(Authentication authentication) {
        var user = (User) authentication.getDetails();
        user = userService.findById(user.getId());
        onlineListService.addUser(user);
        return onlineListService.list();
    }

    @MessageMapping("/board/state/{matchId}")
    @SendTo("/topic/board/state")
    public MatchMessage getBoardState(Authentication authentication, @DestinationVariable Integer matchId) {
        var user = (User) authentication.getDetails();
        Match match = checkBoard(matchId, user);
        var matchMessage = new MatchMessage(match, boardService.getBoard(matchId));
        matchMessage.setData(boardService.getBoard(matchId).serialize());
        System.out.println("Send Board State");
        return matchMessage;
    }

    @MessageMapping("/board/created")
    @SendTo("/topic/board/created")
    public Boolean newMatchCreated() {
        System.out.println("Match Created");
        return true;
    }

    @MessageMapping("/board/start/{matchId}")
    @SendTo("/topic/board/start")
    public StartMessage start(
            Authentication authentication,
            @DestinationVariable Integer matchId) {
        var user = (User) authentication.getDetails();
        checkBoard(matchId, user);
        Match match = matchService.startMatch(matchId, user.getId());
        if (match != null) {
            StartMessage message = new StartMessage(match, boardService.getBoard(matchId));
            message.setMatch(match);
            return message;
        } else {
            return null;
        }
    }


    @MessageMapping("/board/resign/{matchId}")
    @SendTo("/topic/board/resign")
    public ResignMessage resign(
            Authentication authentication,
            @DestinationVariable Integer matchId) {
        var user = (User) authentication.getDetails();
        Match match = checkBoard(matchId, user);
        Match resign = matchService.resign(matchId, user.getId());
        ResignMessage message = new ResignMessage(resign);
        return message;
    }

    @MessageMapping("/board/move/{matchId}")
    @SendTo("/topic/board/move")
    public MoveMessage move(
            Authentication authentication,
            @DestinationVariable Integer matchId, MoveMessage message) {
        System.out.println("Move");
        var user = (User) authentication.getDetails();
        Match match = checkBoard(matchId, user);
        MoveResult move = matchService.move(matchId, user.getId(), message.getI(), message.getJ());
        MoveMessage resultMessage = new MoveMessage(match, boardService.getBoard(matchId));
        resultMessage.setMatch(move.getMatch());
        resultMessage.setMoveResult(move);
        return resultMessage;
    }

    @MessageMapping("/board/chat/{matchId}")
    @SendTo("/topic/board/message")
    public ChatMessage chat(
            Authentication authentication,
            @DestinationVariable Integer matchId, ChatMessage message) {
        System.out.println("Move");
        var user = (User) authentication.getDetails();
        Match match = checkBoard(matchId, user);
        ChatLine chatLine = chatLineService.createChatLine(matchId, user.getId(), message.getContent());
        return message;
    }

    private Match checkBoard(Integer matchId, User user) {
        Match match = matchService.getMatch(matchId);
        if (match == null) {
            throw ServiceException.MATCH_NOT_FOUND;
        }
        if (!match.getPlayerIds().contains(user.getId())) {
            throw ServiceException.MATCH_INVALID_ACCESS;
        }
        return match;
    }
}
