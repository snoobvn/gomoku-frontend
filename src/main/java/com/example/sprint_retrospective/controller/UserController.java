package com.example.sprint_retrospective.controller;


import com.example.sprint_retrospective.ServiceException;
import com.example.sprint_retrospective.dto.SimpleUser;
import com.example.sprint_retrospective.dto.User;
import com.example.sprint_retrospective.dto.VerificationToken;
import com.example.sprint_retrospective.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.persistence.EntityManager;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

@RestController
@RequestMapping("api/user")
@Transactional
public class UserController {


    @Autowired
    private EntityManager entityManager;

    @Autowired
    private UserService userService;

    @GetMapping("profile")
    public SimpleUser profile(Authentication authentication) {
        User user = (User) authentication.getDetails();
        return new SimpleUser(userService.getByEmail(user.getEmail()));
    }

    @PostMapping("updateInfo")
    public User updateInfo(Authentication authentication, @RequestBody ChangeInfoRequest changeInfoRequest) {
        User user = (User) authentication.getDetails();
        return userService.updateInfo(user.getId(), changeInfoRequest.getPassword(),
                changeInfoRequest.getDisplayName());
    }

    @PostMapping("register")
    public User register(@Valid @RequestBody RegisterRequest request) {
        User user = userService.create(request.getDisplayName(), request.getEmail(), request.getPassword());
        return user;
    }

    @GetMapping("check")
    public Object check(Authentication authentication) {
        return authentication != null;
    }

    @GetMapping("/emailconfirm")
    public Boolean confirmRegistration
            (WebRequest request, @RequestParam("token") String token) {

        Locale locale = request.getLocale();

        VerificationToken verificationToken = userService.getVerificationToken(token);
        if (verificationToken == null) {
            return false;
        }

        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            return false;
        }

        user.setEnabled(true);
        userService.enableUser(user);
        return true;
    }

    //Return Auth token on login success
    @GetMapping("login")
    public Object login(String username, String password) {
        String hashedPassword = DigestUtils.md5Hex(password);
        List<User> users = entityManager.createQuery("select u from User u where u.userName = :username", User.class)
                .setParameter("username", username)
                .setMaxResults(1)
                .getResultList();
        if (users.isEmpty()) {
            throw ServiceException.USER_NOT_FOUND;
        }
        User user = users.get(0);
        if (!user.getHashedPassword().equals(hashedPassword)) {
            throw ServiceException.INVALID_PASSWORD;
        }
        String bearer = UUID.randomUUID().toString();
        return bearer;
    }

    @GetMapping
    public List<User> listUsers() {
        return entityManager.createQuery("select u from User u order by u.id desc", User.class)
                .setMaxResults(10).getResultList();
    }
}
