package com.example.sprint_retrospective.controller;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class ChangeInfoRequest {
    private String displayName;
    private String password;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
