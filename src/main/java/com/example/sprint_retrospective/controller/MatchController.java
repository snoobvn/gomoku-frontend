package com.example.sprint_retrospective.controller;

import com.example.sprint_retrospective.controller.message.JoinMessage;
import com.example.sprint_retrospective.dto.ChatLine;
import com.example.sprint_retrospective.dto.Match;
import com.example.sprint_retrospective.dto.SimpleUser;
import com.example.sprint_retrospective.dto.User;
import com.example.sprint_retrospective.service.ChatLineService;
import com.example.sprint_retrospective.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.util.List;

@RestController
@RequestMapping("api/match")
public class MatchController {

    @Autowired
    EntityManager entityManager;

    @Autowired
    MatchService matchService;


    @Autowired
    ChatLineService chatLineService;

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @GetMapping("{id}")
    public Match getMatch(Authentication authentication, @PathVariable Integer id) {
        User user = (User) authentication.getDetails();
        List<Match> matches = entityManager.createQuery("select m from Match m " +
                "where m.id = :id and (m.player1.id = :userId or m.player2.id = :userId)", Match.class)
                .setParameter("userId", user.getId())
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (matches.isEmpty()) {
            return null;
        }
        return matches.get(0);
    }

    @GetMapping
    public List<Match> listMatch(Authentication authentication) {
        User user = (User) authentication.getDetails();
        // List all not ended matches
        return entityManager.createQuery("select m from Match m " +
                        "where m.status <> :status and (m.player1.id <> :userId and (m.player2.id <> :userId or m.player2.id is null)) order by m.createdAt desc",
                Match.class)
                .setParameter("status", Match.MatchStatus.D_ENDED)
                .setParameter("userId", user.getId())
                .getResultList();
    }


    @GetMapping("history")
    public List<Match> history(Authentication authentication) {
        User user = (User) authentication.getDetails();
        // List all ended matches
        return entityManager.createQuery("select m from Match m " +
                        "where m.status = :status and (m.player1.id = :userId or m.player2.id = :userId) order by m.createdAt desc",
                Match.class)
                .setParameter("status", Match.MatchStatus.D_ENDED)
                .setParameter("userId", user.getId())
                .getResultList();
    }

    @PostMapping
    public Match createMatch(Authentication authentication) {
        User user = (User) authentication.getDetails();
        Match match = matchService.createMatch(user.getId());
        if (match != null) {
            simpMessagingTemplate.convertAndSend("/topic/board/created", true);
        }
        return match;
    }


    @DeleteMapping("{matchId}")
    public Boolean removeMatch(@PathVariable Integer matchId, Authentication authentication) {
        User user = (User) authentication.getDetails();
        Boolean matchRemoved = matchService.removeMatch(matchId, user.getId());
        if (matchRemoved != null) {
            simpMessagingTemplate.convertAndSend("/topic/board/removed", true);
        }
        return matchRemoved;
    }

    @PostMapping("{matchId}/join")
    public Match joinMatch(@PathVariable Integer matchId, Authentication authentication) {
        User user = (User) authentication.getDetails();
        Match match = matchService.joinMatch(matchId, user.getId());
        if (match != null) {
            simpMessagingTemplate.convertAndSend("/topic/board/join",
                    new JoinMessage(match, new SimpleUser(user)));
        }
        return match;
    }

    @PostMapping("quickjoin")
    public Match quickJoinMatch(Authentication authentication) {
        User user = (User) authentication.getDetails();
        Match match = matchService.quickJoin(user.getId());
        if (match != null) {
            simpMessagingTemplate.convertAndSend("/topic/board/join",
                    new JoinMessage(match, new SimpleUser(user)));
        }
        return match;
    }

    @GetMapping("{matchId}/chat")
    public List<ChatLine> listChatLine(@PathVariable Integer matchId, Authentication authentication) {
        User user = (User) authentication.getDetails();
        List<ChatLine> chatLines = chatLineService.listChatLine(matchId, 500, 0);
        return chatLines;
    }

    @GetMapping("current")
    public Object currentMatch(Authentication authentication) {
        User user = (User) authentication.getDetails();
        Match match = matchService.currentPlayingMatch(user.getId());
        if (match == null) {
            return false;
        }
        return match;
    }
}
