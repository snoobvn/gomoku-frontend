package com.example.sprint_retrospective.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Board {
    public int[][] board;
    public int size;
    public Integer lastSide = 0;
    private int nWinCell;

    public int[][] fillBoard(int size) {
        this.size = size;
        int[][] arr = new int[size][size];
        for (int i = 0; i < arr.length; i++) {
            Arrays.fill(arr[i], (int) 0);
        }
        return arr;
    }

    public Board(int size, int nWinCell) {
        board = new int[size][size];
        this.nWinCell = nWinCell;
        fillBoard(size);
    }

    public void deserialize(String str) {
        String[] split = str.split(" ");
        int sI = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = Integer.parseInt(split[sI]);
                sI++;
            }
        }
    }

    public MoveResult move(int side, int i, int j) {
        if (i >= size || j >= size) {
            System.out.println("Invalid Cell");
            return new MoveResult(false, null);
        }
        if (side == lastSide) {
            System.out.println("Can't move, wrong turn");
            return new MoveResult(false, null);
        }
        if (board[i][j] != 0) {
            System.out.println("Picked");
            return new MoveResult(false, null);
        }
        board[i][j] = lastSide = side;
        WinResult winResult = detectWinning(side);
        return new MoveResult(true, winResult);
    }

    private WinLine detectHorizonal(int side) {
        int[][] resultBoard = fillBoard(size);
        Integer lastI = null;
        Integer lastJ = null;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                int v = board[i][j];
                if (v == side) {
                    if (j == 0) {
                        resultBoard[i][j] = 1;
                    } else {
                        resultBoard[i][j] += resultBoard[i][j - 1] + 1;
                    }
                    if (resultBoard[i][j] == nWinCell) {
                        lastI = i;
                        lastJ = j;
                    }
                    if (resultBoard[i][j] > nWinCell) {
                        lastI = null;
                        lastJ = null;
                    }
                }
            }
        }
        if (lastI != null) {
            return new WinLine(WinLine.HORIZONTAL, lastI, lastJ);
        }
        return null;
    }

    private WinLine detectVertical(int side) {
        int[][] resultBoard = fillBoard(size);
        Integer lastI = null;
        Integer lastJ = null;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                int v = board[i][j];
                if (v == side) {
                    if (i == 0) {
                        resultBoard[i][j] = 1;
                    } else {
                        resultBoard[i][j] += resultBoard[i - 1][j] + 1;
                    }
                    if (resultBoard[i][j] == nWinCell) {
                        lastI = i;
                        lastJ = j;
                    }
                    if (resultBoard[i][j] > nWinCell) {
                        lastI = null;
                        lastJ = null;
                    }
                }
            }
        }
        if (lastI != null) {
            return new WinLine(WinLine.VERTICAL, lastI, lastJ);
        }
        return null;
    }

    private WinLine detectDiagonal(int side) {
        int[][] resultBoard = fillBoard(size);
        Integer lastI = null;
        Integer lastJ = null;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                int v = board[i][j];
                if (v == side) {
                    if (i == 0 || j == 0) {
                        resultBoard[i][j] = 1;
                    } else {
                        resultBoard[i][j] += resultBoard[i - 1][j - 1] + 1;
                    }
                    if (resultBoard[i][j] == nWinCell) {
                        lastI = i;
                        lastJ = j;
                    }
                    if (resultBoard[i][j] > nWinCell) {
                        lastI = null;
                        lastJ = null;
                    }
                }
            }
        }
        if (lastI != null) {
            return new WinLine(WinLine.DIAGONAL, lastI, lastJ);
        }
        return null;
    }

    private WinLine detectMinorDiagonal(int side) {
        int[][] resultBoard = fillBoard(size);
        Integer lastI = null;
        Integer lastJ = null;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                int v = board[i][j];
                if (v == side) {
                    if (i == 0 || j == size - 1) {
                        resultBoard[i][j] = 1;
                    } else {
                        resultBoard[i][j] += resultBoard[i - 1][j + 1] + 1;
                    }
                    if (resultBoard[i][j] == nWinCell) {
                        lastI = i;
                        lastJ = j;
                    }
                    if (resultBoard[i][j] > nWinCell) {
                        lastI = null;
                        lastJ = null;
                    }
                }
            }
        }
        if (lastI != null) {
            return new WinLine(WinLine.MINOR_DIAGONAL, lastI, lastJ);
        }
        return null;
    }

    private boolean isCellAvailable() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == 0) {
                    return true;
                }

            }
        }
        return false;
    }

    public WinResult detectWinning(int side) {
        List<WinLine> lines = new ArrayList<WinLine>(4);
        WinLine verticalLine = detectVertical(side);
        if (verticalLine != null) {
            lines.add(verticalLine);
        }

        WinLine horizontalLine = detectHorizonal(side);
        if (horizontalLine != null) {
            lines.add(horizontalLine);
        }

        WinLine diagonalLine = detectDiagonal(side);
        if (diagonalLine != null) {
            lines.add(diagonalLine);
        }

        WinLine minorDiagonalLine = detectMinorDiagonal(side);
        if (minorDiagonalLine != null) {
            lines.add(minorDiagonalLine);
        }

        if (lines.isEmpty()) {
            if (isCellAvailable()) {
                return null;
            } else {
                return new WinResult();
            }
        } else {
            return new WinResult(side, lines);
        }
    }

    public String serialize() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                int v = board[i][j];
                str.append(v);
                str.append(" ");
            }
        }
        return str.toString();
    }

    public void printBoard() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.printf("%1d ", board[i][j]);
            }
            System.out.println();
        }
    }
}
