package com.example.sprint_retrospective.logic;

public class WinLine {
	public static int HORIZONTAL = 0;
	public static int VERTICAL = 1;
	public static int DIAGONAL = 2;
	public static int MINOR_DIAGONAL = 3;
	public int type;
	public int x;
	public int y;

	public WinLine(int type, int x, int y) {
		super();
		this.type = type;
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WinLine [type=");
		builder.append(type);
		builder.append(", x=");
		builder.append(x);
		builder.append(", y=");
		builder.append(y);
		builder.append("]");
		return builder.toString();
	}

}
