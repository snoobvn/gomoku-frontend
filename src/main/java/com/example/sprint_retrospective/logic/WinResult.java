package com.example.sprint_retrospective.logic;

import java.util.Arrays;
import java.util.List;

public class WinResult {

	//Size == 0 if draw
	Integer side;
	WinLine[] lines;

	//Draw
	public WinResult() {
		side = 0;
	}

	public WinResult(Integer side, List<WinLine> lines) {
		super();
		this.side = side;
		this.lines = (WinLine[]) lines.toArray(new WinLine[] {});
	}

	public WinResult(Integer side, WinLine[] lines) {
		super();
		this.side = side;
		this.lines = lines;
	}

	public Integer getSide() {
		return side;
	}

	public void setSide(Integer side) {
		this.side = side;
	}

	public WinLine[] getLines() {
		return lines;
	}

	public void setLines(WinLine[] lines) {
		this.lines = lines;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WinResult [side=");
		builder.append(side);
		builder.append(", lines=");
		builder.append(Arrays.toString(lines));
		builder.append("]");
		return builder.toString();
	}

}
