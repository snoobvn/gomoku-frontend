package com.example.sprint_retrospective;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestControllerAdvice
class GlobalControllerExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)  // 409
    @ExceptionHandler(ServiceException.class)
    public ServiceException.ServiceExceptionResponse handleConflict(ServiceException errorResponse) {
        return new ServiceException.ServiceExceptionResponse(errorResponse);
    }

}