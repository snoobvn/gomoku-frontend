package com.example.sprint_retrospective.app;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class MyUserDetailService implements UserDetailsService {
    final
    EntityManager entityManager;

    public MyUserDetailService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        List<com.example.sprint_retrospective.dto.User> users = entityManager.createQuery(
                "SELECT u FROM User u WHERE u.email = :userName",
                com.example.sprint_retrospective.dto.User.class).setParameter("userName", username).getResultList();
        if (users.isEmpty()) throw new UsernameNotFoundException("User Not Found");
        var user = users.get(0);
        return toUserDetails(user);
    }
    public User toUserDetails(com.example.sprint_retrospective.dto.User user){
        return new User(user.getEmail(), user.getHashedPassword(), user.getEnabled(), true, true, !user.getLocked(),
                AuthorityUtils.createAuthorityList("ROLE_USER"));
    }
}
