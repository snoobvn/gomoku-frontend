package com.example.sprint_retrospective.app;

import com.example.sprint_retrospective.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.web.OAuth2LoginAuthenticationFilter;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletResponse;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final String successUrl = "/api/user/profile";
    @Autowired
    MyUserDetailService userDetailsService;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    CustomAuthTokenFilter customAuthTokenFilter;
    @Autowired
    ExceptionHandlerFilter exceptionHandlerFilter;
    @Autowired
    CORSFilter corsFilter;

    @Bean
    @Qualifier("myAuth")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> uds() {
        return token -> userDetailsService.toUserDetails((User) token.getPrincipal());
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        PreAuthenticatedAuthenticationProvider authenticationProvider = new PreAuthenticatedAuthenticationProvider();
        authenticationProvider.setThrowExceptionWhenTokenRejected(false);
        authenticationProvider.setPreAuthenticatedUserDetailsService(uds());
        return authenticationProvider;
    }

    @Override
    @Autowired
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider())
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
    }

    protected void configure(HttpSecurity http) throws Exception {
        http
                .exceptionHandling()
                .defaultAuthenticationEntryPointFor(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED),
                        new AntPathRequestMatcher("/api/**")).and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/api/user/register").permitAll()
                .antMatchers("/login/**").permitAll()
//                .antMatchers("/api/user/check").permitAll()
                .antMatchers("/api/user/updateInfo").permitAll()
                .antMatchers("/api/user/emailconfirm").permitAll()
                .anyRequest().authenticated()
                .and().formLogin()
                .permitAll()
                .successHandler(new HTTPStatusHandler(HttpStatus.OK))
                .defaultSuccessUrl(successUrl)
                .and().oauth2Login().permitAll()
                .defaultSuccessUrl(successUrl)
                .and()
                .logout().logoutSuccessHandler(new HTTPStatusHandler(HttpStatus.OK))
                .and()
                .addFilterAfter(customAuthTokenFilter, OAuth2LoginAuthenticationFilter.class)
                .addFilterBefore(exceptionHandlerFilter, customAuthTokenFilter.getClass());
    }
}