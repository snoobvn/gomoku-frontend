package com.example.sprint_retrospective.service;

import com.example.sprint_retrospective.dto.User;
import com.example.sprint_retrospective.dto.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository
  extends JpaRepository<User, Integer> {

    User findByEmail(String email);
}