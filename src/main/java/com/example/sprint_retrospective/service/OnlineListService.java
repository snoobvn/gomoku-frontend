package com.example.sprint_retrospective.service;

import com.example.sprint_retrospective.dto.SimpleUser;
import com.example.sprint_retrospective.dto.User;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class OnlineListService {

    private Map<User, Long> users = new LinkedHashMap<>();

    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public OnlineListService() {
        executorService.scheduleWithFixedDelay(this::clearDisconnectedUsers, 5, 5, TimeUnit.SECONDS);
    }

    public Set<SimpleUser> list() {
        return users.keySet().stream().map(SimpleUser::new)
                .sorted(Comparator.comparing(SimpleUser::getScore, Comparator.reverseOrder()))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public void addUser(User user) {
        users.put(user, System.currentTimeMillis());
    }

    public void removeUser(User user) {
        users.remove(user);
    }

    private void clearDisconnectedUsers() {
        users = users.entrySet().stream().filter(es -> (System.currentTimeMillis() - es.getValue() < 5000)).collect(
                Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
