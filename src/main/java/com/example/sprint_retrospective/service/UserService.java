package com.example.sprint_retrospective.service;

import com.example.sprint_retrospective.ServiceException;
import com.example.sprint_retrospective.dto.User;
import com.example.sprint_retrospective.dto.VerificationToken;
import com.example.sprint_retrospective.event.OnRegistrationCompleteEvent;
import com.example.sprint_retrospective.util.SpringUtils;
import com.nimbusds.oauth2.sdk.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
@Transactional
public class UserService {
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private VerificationTokenRepository tokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    public User findById(Integer integer) {
        return userRepository.findById(integer).get();
    }

    public User create(String displayName, String email, String rawPassword) {
        return create(displayName, email, rawPassword, false);
    }

    public User updateInfo(Integer userId, String newPassword, String displayName) {
        Optional<User> oUser = userRepository.findById(userId);
        var user = oUser.get();
        if (!StringUtils.isBlank(newPassword)) {
            user.setPasswordChanged(true);
            user.setHashedPassword(passwordEncoder.encode(newPassword));
        }
        if (!StringUtils.isBlank(displayName)) {
            user.setDisplayName(displayName);
        }
        return userRepository.save(user);
    }
    public User create(String displayName, String email, String rawPassword, Boolean isAutoRegister) {
        System.out.println("Created Called");
        String hashedPassword = passwordEncoder.encode(rawPassword);
        List<User> users = entityManager.createQuery("select u from User u where u.email = :email", User.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList();
        if (users.size() > 0) {
            throw ServiceException.USER_EXIST;
        }
        User createdUser = new User(displayName, email, hashedPassword);
        if (isAutoRegister) {
            createdUser.setPasswordChanged(false);
            createdUser.setEnabled(true);
        }
        userRepository.saveAndFlush(createdUser);
        Optional<HttpServletRequest> request = SpringUtils.getCurrentHttpRequest();

        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(createdUser,
                request.map(ServletRequest::getLocale).orElse(Locale.ENGLISH),
                request.map(HttpServletRequest::getContextPath).orElse(""),
                isAutoRegister ? rawPassword : null
        ));
        return createdUser;
    }

    public User getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    private boolean emailExist(String email) {
        return userRepository.findByEmail(email) != null;
    }

    public VerificationToken getVerificationToken(String VerificationToken) {
        return tokenRepository.findByToken(VerificationToken);
    }

    public void createVerificationToken(User user, String token) {
        VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
    }

    public User enableUser(User user) {
        user.setEnabled(true);
        return userRepository.save(user);
    }
}
