package com.example.sprint_retrospective.service;

import com.example.sprint_retrospective.dto.User;
import com.example.sprint_retrospective.dto.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepository
  extends JpaRepository<VerificationToken, Long> {

    VerificationToken findByToken(String token);

    VerificationToken findByUser(User user);
}