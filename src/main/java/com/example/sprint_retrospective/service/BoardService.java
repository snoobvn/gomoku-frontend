package com.example.sprint_retrospective.service;

import com.example.sprint_retrospective.Constant;
import com.example.sprint_retrospective.dto.Match;
import com.example.sprint_retrospective.logic.Board;
import com.example.sprint_retrospective.logic.MoveResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static com.example.sprint_retrospective.ServiceException.MATCH_NOT_FOUND;

@Service
public class BoardService {
    @Autowired
    private MatchService matchService;
    private final Map<Integer, Board> boardMatch = new HashMap<>();

    public BoardService() {
    }

    public Board getBoard(Integer matchId) {
        //Find playing board
        if (boardMatch.containsKey(matchId)) {
            return boardMatch.get(matchId);
        }

        // Find ended
        Match match = matchService.getMatch(matchId);
        if (match == null) {
            throw MATCH_NOT_FOUND;
        }
        return boardMatch.computeIfAbsent(matchId, id -> {
            Board board = new Board(Constant.boardSize, Constant.winCell);
            if (match.getData() != null) {
                board.deserialize(match.getData());
            }
            return board;
        });
    }

    public MoveResult move(Integer matchId, int side, int i, int j) {
        return getBoard(matchId).move(side, i, j);
    }
}
