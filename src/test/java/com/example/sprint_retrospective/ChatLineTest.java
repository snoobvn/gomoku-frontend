package com.example.sprint_retrospective;

import com.example.sprint_retrospective.controller.UserController;
import com.example.sprint_retrospective.dto.Match;
import com.example.sprint_retrospective.dto.User;
import com.example.sprint_retrospective.logic.Board;
import com.example.sprint_retrospective.service.BoardService;
import com.example.sprint_retrospective.service.MatchService;
import com.example.sprint_retrospective.service.ChatLineService;
import com.example.sprint_retrospective.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@SpringBootTest
@Transactional
class ChatLineTest {

    @Autowired
    ChatLineService chatLineService;
    @Autowired
    MatchService matchService;
    @Autowired
    BoardService boardService;
    @Autowired
    EntityManager em;
    @Autowired
    UserService userService;

    @Autowired
    @Test
    public void testNewUser() {
        userService.create("Quang", "snoobvn@gmail.com", "Hello World", false);
    }

    @Test
    public void chatLineCreate() {
        User player1 = userService.create("Quang", "test", "test", true);
        User player2 = userService.create("Quang", "test2", "test", true);
        Match match = matchService.createMatch(player1.getId());
        matchService.joinMatch(match.getId(), player2.getId());
        matchService.startMatch(match.getId(), player1.getId());
        matchService.move(match.getId(), player1.getId(), 1, 1);
        matchService.move(match.getId(), player2.getId(), 1, 8);
        matchService.move(match.getId(), player1.getId(), 2, 1);
        matchService.move(match.getId(), player2.getId(), 1, 3);
        matchService.move(match.getId(), player1.getId(), 3, 1);
        matchService.move(match.getId(), player2.getId(), 1, 4);
        matchService.move(match.getId(), player1.getId(), 4, 1);
        matchService.move(match.getId(), player2.getId(), 1, 5);
        matchService.move(match.getId(), player1.getId(), 5, 1);
        matchService.move(match.getId(), player2.getId(), 1, 6);
        boardService.getBoard(match.getId()).printBoard();
    }
}
